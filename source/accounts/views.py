from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LogoutView, LoginView
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from accounts.forms import UserCreationForm, UserChangeForm


class UserListView(PermissionRequiredMixin, ListView):
    model = User
    template_name = 'user/list.html'
    context_object_name = 'users'
    permission_required = ['auth.view_user']
    permission_denied_message = ['Доступ ограничен']


class UserLoginView(LoginView):
    template_name = 'user/login.html'


class UserLogoutView(LoginRequiredMixin, LogoutView):
    next_page = 'webapp:list_articles'


class UserCreateView(PermissionRequiredMixin, CreateView):
    model=User
    template_name = 'user/create.html'
    form_class = UserCreationForm
    permission_required = ['auth.add_user']
    permission_denied_message = ['Доступ ограничен']

    def get_success_url(self):
        return reverse('accounts:list_users')


class UserChangeView(UserPassesTestMixin, UpdateView):
    model = User
    template_name = 'user/change.html'
    form_class = UserChangeForm
    context_object_name = 'user_obj'

    def get_success_url(self):
        return reverse('webapp:list_articles')

    def get_user(self):
        user_pk = self.kwargs['pk']
        return get_object_or_404(User, pk=user_pk)

    def test_func(self):
        edited_user = self.get_user()
        return self.request.user.has_perm('auth.change_user') or self.request.user.id == edited_user.id


class UserDeleteView(UserPassesTestMixin, DeleteView):
    model= User
    template_name = 'user/delete.html'
    context_object_name = 'user_obj'

    def get_success_url(self):
        return reverse('accounts:list_users')

    def get_user(self):
        user_pk = self.kwargs['pk']
        return get_object_or_404(User, pk=user_pk)

    def test_func(self):
        deleted_user = self.get_user()
        return self.request.user.has_perm('auth.delete_user') or self.request.user.id == deleted_user.id