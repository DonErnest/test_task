from django.urls import path

from accounts.views import UserListView, UserLogoutView, UserLoginView, UserCreateView, UserChangeView, UserDeleteView

urlpatterns = [
    path('users/register/', UserCreateView.as_view(), name='register_user'),
    path('users/<int:pk>/edit/', UserChangeView.as_view(), name='change_user'),
    path('users/login/', UserLoginView.as_view(), name='login'),
    path('users/logout/', UserLogoutView.as_view(), name='logout'),
    path('users/', UserListView.as_view(), name='list_users'),
    path('users/<int:pk>/delete/', UserDeleteView.as_view(), name='delete_user'),
]

app_name = 'accounts'