from django.contrib import admin

from webapp.models import Category, Article


class CategoryModelAdmin(admin.ModelAdmin):
    fields=['title', 'parent_id']
    list_display = ['id', 'title', 'parent_id']
    list_filter = ['parent_id']
    list_display_links = ['id']
    search_fields = ['title']


class ArticleModelAdmin(admin.ModelAdmin):
    fields = ['category_id','user_id','title','description','image']
    list_display = ['id', 'category_id','user_id','title','description','image']
    list_filter = ['category_id', 'user_id']
    list_display_links = ['id', 'title']
    search_fields = ['title','description']


admin.site.register(Category, CategoryModelAdmin)
admin.site.register(Article, ArticleModelAdmin)