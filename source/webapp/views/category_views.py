from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse
from django.views.generic import ListView, UpdateView

from webapp.forms import CategoryForm
from webapp.models import Category


class CategoryListView(PermissionRequiredMixin, ListView):
    model = Category
    template_name = 'category/list.html'
    context_object_name = 'categories'
    paginate_by = 10
    paginate_orphans = 2
    permission_required = ['webapp.view_category']
    permission_denied_message = ['Доступ ограничен']
    ordering = ['title']

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        context['form'] = CategoryForm
        return context


class CategoryEditView(PermissionRequiredMixin, UpdateView):
    model = Category
    fields = ['title', 'parent_id']
    template_name = 'category/category_edit.html'
    permission_required = ['webapp.change_category']
    permission_denied_message = ['Доступ ограничен']

    def get_success_url(self):
        return reverse('webapp:list_categories')
