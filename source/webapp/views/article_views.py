from django.contrib.auth.mixins import PermissionRequiredMixin, UserPassesTestMixin
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.views import View
from django.views.generic import ListView, UpdateView, CreateView

from webapp.forms import ArticleFormForAuthor, ArticleFormForStaff
from webapp.models import Article


class ArticleListView(PermissionRequiredMixin, ListView):
    model = Article
    template_name = 'article/list.html'
    context_object_name = 'articles'
    paginate_by = 10
    paginate_orphans = 2
    permission_required = ['webapp.view_article']
    permission_denied_message = ['Доступ ограничен']


class ArticleEditView(UserPassesTestMixin, UpdateView):
    model=Article
    form_class = ArticleFormForAuthor
    template_name = 'article/edit.html'

    def form_valid(self, form):
        self.object = form.save()
        self.object.user_id = self.request.user
        return super(ArticleEditView, self).form_valid(form)

    def get_form(self, form_class=None):
        if self.request.user.has_perm('auth.view_user'):
            self.form_class = ArticleFormForStaff
        return super(ArticleEditView, self).get_form(form_class)

    def get_success_url(self):
        return reverse('webapp:list_articles')

    def get_article(self):
        article_pk = self.kwargs['pk']
        return get_object_or_404(Article, pk=article_pk)

    def test_func(self):
        article = self.get_article()
        return self.request.user.has_perm('webapp.change_article') or self.request.user.id == article.user_id.id


class ArticleDeleteView(PermissionRequiredMixin, View):
    permission_required = ['webapp.delete_article']
    permission_denied_message = ['Доступ ограничен']

    def get(self, request, *args, **kwargs):
        article = self.get_article()
        article.delete()
        return redirect('webapp:list_articles')

    def get_article(self):
        article_pk = self.kwargs['pk']
        return get_object_or_404(Article, pk=article_pk)


class ArticleCreateView(PermissionRequiredMixin, CreateView):
    model = Article
    form_class = ArticleFormForAuthor
    template_name = 'article/create.html'
    permission_required = ['webapp.add_article']
    permission_denied_message = ['Доступ ограничен']

    def get_success_url(self):
        return reverse('webapp:list_articles')

    def get_form(self, form_class=None):
        if self.request.user.has_perm('auth.view_user'):
            self.form_class = ArticleFormForStaff
        return super(ArticleCreateView, self).get_form(form_class)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            self.object = form.save()
            if not request.user.has_perm('webapp.view_user'):
                self.object.user_id = request.user
                self.object.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)