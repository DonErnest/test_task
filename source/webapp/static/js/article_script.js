const baseUrl = 'http://localhost:8000/articles/';

function setUpRemoveLinks(){
    let deleteButtonsArray = $('.delete-button');
    let deleteLink = $('#deleteArticle');
    if(deleteButtonsArray.length > 0){
        deleteButtonsArray.each(function(index) {
            $(this).on('click', function(event){
                let deleteLink = $('#deleteArticle');
                deleteLink.removeAttr('href');
                deleteLink.attr('href',getFullPath(`/${$(this).data('article-id')}/delete/`))
            })
        })
    }
}

function getFullPath(path) {
    path = path.replace(/^\/+|\/+$/g, '');
    path = path.replace(/\/{2,}/g, '/');
    return baseUrl + path;
}


$(document).ready(function() {
    setUpRemoveLinks();
});