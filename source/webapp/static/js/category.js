const baseUrl = 'http://localhost:8000/api/v1/';

function setUpCategoryDeleteButton(button){
    button.on('click', function(event){
        event.preventDefault();
        makeRequest(`/categories/${button.data('id')}/delete`, 'delete').done(function(data, message, status){
            let parentItem = $(`#category_item_${button.data('id')}`);
            console.log('success');
            console.log(parentItem);
            parentItem.remove();
            $(`select option[value=${button.data('id')}]`).remove()
        }).fail(function(status, message, response){
            console.log(status);
            console.log(message);
            console.log(response)
        })
    })
}

function setUpCategoryDeleteButtons() {
    let deleteButtonsArray = $('.button-delete');
    if(deleteButtonsArray.length > 0){
        deleteButtonsArray.each(function(index) {
            // console.log($(this));
            setUpCategoryDeleteButton($(this));
        })
    }
}


function appendCategoryList(beforeElement, data, selector){
    if (data.parent_id !== null ) {
        let newCategoryItem = $(`<li class="list-group-item d-flex justify-content-between align-items-center" id="category_item_${data.id}">
                <p>${data.title} (основная категория <strong>${data.parent_id.title }</strong>)</p>
                <div class="btn-group">
                    <button class="btn btn-success" data-category-edit="${data.id}" data-toggle="modal" data-target="#exampleModal">Редактировать</button>
                    <button id="category_delete_${data.id}" class="btn btn-warning" data-id="${data.id}">Удалить</button>
                </div>
            </li>`);
        newCategoryItem.insertBefore(beforeElement);
    } else {
        let newCategoryItem = $(`<li class="list-group-item d-flex justify-content-between align-items-center" id="category_item_${data.id}">
                <p>${data.title}</p>
                <div class="btn-group">
                    <button class="btn btn-success" data-category-edit="${data.id}" data-toggle="modal" data-target="#exampleModal">Редактировать</button>
                    <button id="category_delete_${data.id}" class="btn btn-warning" data-id="${data.id}">Удалить</button>
                </div>
            </li>`);
        newCategoryItem.insertBefore(beforeElement);
    }
}

function setUpAddCategoryForm(){
    let collapseCategoryFormItem = $('#collapseFormItem');
    let addCategoryButton = $('#addCategoryFormSubmit');
    addCategoryButton.on('click', function (event) {
        event.preventDefault();
        let newCategoryParent = $('#id_parent_id');
        let newCategoryTitle = $('#id_title');
        let data = {
                "title": newCategoryTitle.val(),
            };
        if(newCategoryParent.children("option:selected").text().replace(/-/g, "") !== "" ) {
            data["parent_id"] = {"title": newCategoryParent.children("option:selected").text()};
        }
        makeRequest('/categories/add/', 'post', data).done(function(data, status, message){
            console.log(message);
            console.log(status);
            console.log(data);
            appendCategoryList(collapseCategoryFormItem, data, newCategoryParent);
            let newDeleteButton = $(`#category_delete_${data.id}`);
            setUpCategoryDeleteButton(newDeleteButton);
            newCategoryParent.append(`<option value="${data.id}">${data.title}</option>`)
        }).fail(function (status, message, response) {
            console.log(message);
            console.log(status);
            console.log(response)
        })
    })
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function getFullPath(path) {
    path = path.replace(/^\/+|\/+$/g, '');
    path = path.replace(/\/{2,}/g, '/');
    return baseUrl + path;
}

function makeRequest(path, method, data=null) {
    let settings = {
        url: getFullPath(path),
        method: method,
        dataType: 'json',
        headers: {'X-CSRFToken': getCookie('csrftoken')},
    };
    if (data) {
        settings['data'] = JSON.stringify(data);
        settings['contentType'] = 'application/json';
    }
    return $.ajax(settings);
}

$(document).ready(function() {
    setUpAddCategoryForm();
    setUpCategoryDeleteButtons();
});