from django.urls import path

from webapp.views.article_views import ArticleListView, ArticleEditView, ArticleDeleteView, ArticleCreateView
from webapp.views.category_views import CategoryListView, CategoryEditView

urlpatterns = [
    path('categories/', CategoryListView.as_view(), name='list_categories'),
    path('categories/<int:pk>/edit/', CategoryEditView.as_view(), name='edit_category'),
    path('', ArticleListView.as_view(), name='list_articles'),
    path('articles/add/', ArticleCreateView.as_view(), name='add_article'),
    path('articles/<int:pk>/edit/', ArticleEditView.as_view(), name='edit_article'),
    path('articles/<int:pk>/delete/', ArticleDeleteView.as_view(), name='delete_article'),
]

app_name = 'webapp'