from django import forms

from webapp.models import Category, Article


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['title', 'parent_id']


class ArticleFormForAuthor(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'category_id', 'description', 'image']


class ArticleFormForStaff(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'category_id', 'user_id', 'description', 'image']