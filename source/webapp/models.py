from django.contrib.auth.models import User
from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=30, verbose_name='Название категории')
    parent_id = models.ForeignKey('self', null=True, blank=True, verbose_name='Название основной категории',
                                  on_delete=models.PROTECT)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Categories'

class Article(models.Model):
    category_id = models.ForeignKey('webapp.Category', verbose_name='Категория', on_delete=models.PROTECT)
    user_id = models.ForeignKey(User, verbose_name='Автор статьи', null=True, blank=True, on_delete=models.SET_NULL)
    title = models.CharField(max_length=50, verbose_name='Название статьи')
    description = models.TextField(max_length=3000, verbose_name='Текст статьи')
    image = models.ImageField(upload_to='articles_images/', verbose_name='Аватар статьи')

    def __str__(self):
        if self.user_id:
            return "Статья {} от {}".format(self.title, self.user_id.username)
        else:
            return "Статья {}".format(self.title)