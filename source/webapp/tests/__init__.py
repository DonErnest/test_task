from .category_tests import CategoryListView, CategoryEditViewTestCase
from .article_tests import ArticleListView, ArticleCreateViewTestCase, ArticleEditViewTestCase