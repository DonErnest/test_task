from django.contrib.auth import authenticate
from django.contrib.auth.models import User, Permission, Group
from django.test import TestCase
from django.urls import reverse
from webapp.models import Category


class CategoryListView(TestCase):
    fixtures = ['test_auth_data.json']

    def setUp(self):
        number_or_categories = 15
        for category_num in range(number_or_categories):
            Category.objects.create(title='Category %s' % category_num)

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get(reverse('webapp:list_categories'))
        self.assertRedirects(resp, '/accounts/users/login/?next=/categories/')

    def test_view_url_exists_at_desired_location(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get('/categories/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:list_categories'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:list_categories'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'category/list.html')

    def test_pagination_is_ten(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:list_categories'))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(resp.context['is_paginated'] == True)
        self.assertTrue(len(resp.context['object_list']) == 10)

    def test_lists_all_categories(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:list_categories') + '?page=2')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(resp.context['is_paginated'] == True)
        self.assertTrue(len(resp.context['object_list']) == 5)


class CategoryEditViewTestCase(TestCase):
    fixtures = ['test_auth_data.json']

    def setUp(self):
        number_or_categories = 15
        for category_num in range(number_or_categories):
            Category.objects.create(title='Category %s' % category_num)

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get(reverse('webapp:edit_category', kwargs={'pk':3}))
        self.assertRedirects(resp, '/accounts/users/login/?next=/categories/3/edit/')

    def test_view_url_exists_at_desired_location(self):
        login = self.client.login(username='Contentmanager', password='unabomber45')
        resp = self.client.get('/categories/3/edit/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        login = self.client.login(username='Contentmanager', password='unabomber45')
        resp = self.client.get(reverse('webapp:edit_category', kwargs={'pk':3}))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        login = self.client.login(username='Contentmanager', password='unabomber45')
        resp = self.client.get(reverse('webapp:edit_category', kwargs={'pk':3}))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'category/category_edit.html')

    def test_edited_category(self):
        login = self.client.login(username='Contentmanager', password='unabomber45')
        category = self.client.post('/categories/3/edit/', {'title': "NewCategory", 'parent_id': 4,})
        response = self.client.get(reverse('webapp:list_categories'))
        edited_category = Category.objects.get(pk=3)
        self.assertTrue(edited_category.title, "NewCategory")
        self.assertTrue(edited_category.parent_id.title, 'Category 4s')

    def test_edit_inaccessible_without_permission(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        category = self.client.post('/categories/3/edit/', {'title': "NewCategory", 'parent_id': 4, })
        self.assertEqual(category.status_code, 403)
