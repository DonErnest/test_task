from django.contrib.auth.models import User, Group
from django.test import TestCase
from django.urls import reverse
from webapp.models import Category, Article


class ArticleListView(TestCase):
    fixtures = ['test_auth_data.json']

    def setUp(self):
        number_of_articles = 15

        category = Category.objects.create(title='TestCategoryForArticle')
        for article_num in range(number_of_articles):
            Article.objects.create(title='Category %s' % article_num, category_id=category, description='Blablablabla',
                                   image='images/vasserman.jpeg', user_id=User.objects.get(username='unabomber'))

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get(reverse('webapp:list_articles'))
        self.assertRedirects(resp, '/accounts/users/login/?next=/')

    def test_view_url_exists_at_desired_location(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get('')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:list_articles'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:list_articles'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'article/list.html')

    def test_pagination_is_ten(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:list_articles'))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(resp.context['is_paginated'] == True)
        self.assertTrue(len(resp.context['object_list']) == 10)

    def test_lists_all_categories(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:list_articles') + '?page=2')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(resp.context['is_paginated'] == True)
        self.assertTrue(len(resp.context['object_list']) == 5)


class ArticleCreateViewTestCase(TestCase):
    fixtures = ['test_auth_data.json']

    def setUp(self):
        number_of_articles = 15
        category = Category.objects.create(title='TestCategoryForArticle')

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get(reverse('webapp:add_article'))
        self.assertRedirects(resp, '/accounts/users/login/?next=/articles/add/')

    def test_view_url_exists_at_desired_location(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get('/articles/add/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:add_article'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:add_article'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'article/create.html')

    def test_post_article(self):
        user = User.objects.get(username='unabomber')
        login = self.client.login(username='unabomber', password='unabomber67')
        with open('webapp/static/images/vasserman.jpeg', 'rb') as img:
            post = self.client.post('/articles/add/', {'title': "NewArticle", "category_id" : 1,
                                                       "description" :'Blablablablabla', "image": img})
        self.assertEqual(post.status_code, 302)
        self.assertTrue(Article.objects.all().first().title, 'NewArticle')


class ArticleEditViewTestCase(TestCase):
    fixtures = ['test_auth_data.json']

    def setUp(self):
        number_of_articles = 15

        category = Category.objects.create(title='TestCategoryForArticle')
        for article_num in range(number_of_articles):
            Article.objects.create(title='Category %s' % article_num, category_id=category, description='Blablablabla',
                                   image='images/vasserman.jpeg', user_id=User.objects.get(username='unabomber'))

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get(reverse('webapp:edit_article', kwargs={'pk': 1}))
        self.assertRedirects(resp, '/accounts/users/login/?next=/articles/1/edit/')

    def test_view_url_exists_at_desired_location(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get('/articles/1/edit/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:edit_article', kwargs={'pk': 1}))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get(reverse('webapp:edit_article', kwargs={'pk': 1}))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'article/edit.html')

    def test_post_edit_article(self):
        user = User.objects.get(username='unabomber')
        login = self.client.login(username='unabomber', password='unabomber67')
        post = self.client.post('/articles/1/edit/', {'title': "Иное название", 'description': 'Иное описание'})
        response = self.client.get(reverse('webapp:list_articles'))
        edited_article = Article.objects.get(pk=1)
        self.assertTrue(edited_article.title, "Иное название")
        self.assertTrue(edited_article.description, 'Иное описание')


class ArticleDeleteViewTestCase(TestCase):
    fixtures = ['test_auth_data.json']

    def setUp(self):
        number_of_articles = 15
        category = Category.objects.create(title='TestCategoryForArticle')
        for article_num in range(number_of_articles):
            Article.objects.create(title='Category %s' % article_num, category_id=category, description='Blablablabla',
                                   image='images/vasserman.jpeg', user_id=User.objects.get(username='unabomber'))

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get(reverse('webapp:delete_article', kwargs={'pk': 1}))
        self.assertRedirects(resp, '/accounts/users/login/?next=/articles/1/delete/')

    def test_delete_article(self):
        old_articles_list = Article.objects.all()
        login = self.client.login(username='Contentmanager', password='unabomber45')
        resp = self.client.get('/articles/1/delete/')
        self.assertEqual(resp.status_code, 302)
        new_articles_list = Article.objects.all()
        self.assertTrue(len(old_articles_list)-1, len(new_articles_list))

    def test_delete_with_no_permission(self):
        old_articles_list = Article.objects.all()
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get('/articles/1/delete/')
        self.assertEqual(resp.status_code, 403)
        new_articles_list = Article.objects.all()
        self.assertTrue(len(old_articles_list), len(new_articles_list))