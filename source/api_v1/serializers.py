from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework import serializers

from webapp.models import Category, Article



class ParentCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'title')


class CategorySerializer(serializers.ModelSerializer):
    parent_id = ParentCategorySerializer(required=False, allow_null=True)

    class Meta:
        model = Category
        fields = ('id', 'title', 'parent_id')

    def create(self, validated_data):
        parent_category_data = validated_data.pop('parent_id', None)
        category = Category.objects.create(**validated_data)
        if parent_category_data:
            parent = get_object_or_404(Category, title=parent_category_data['title'])
            category.parent_id = parent
            category.save()
        return category


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class ArticleSerializer(serializers.ModelSerializer):
    category_id = CategorySerializer(read_only=True)
    user_id = UserSerializer(read_only=True)

    class Meta:
        model=Article
        fields = ('category_id', 'user_id', 'title', 'description', 'image')