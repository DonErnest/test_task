from django.urls import path

from api_v1.views import CategoryAddView, CategoryDeleteView, CategoryListView, ArticleListView

urlpatterns = [
    path('categories/add', CategoryAddView.as_view(), name='create_category'),
    path('categories/<int:pk>/delete/', CategoryDeleteView.as_view(), name='delete_category'),
    path('categories/', CategoryListView.as_view(), name='list_categories'),
    path('articles/', ArticleListView.as_view(), name='list_articles'),
]

app_name ='api_v1'