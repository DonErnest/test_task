from django.contrib.auth.models import Group, User
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework.utils import json

from api_v1.serializers import CategorySerializer, ArticleSerializer
from webapp.models import Category, Article


class CategoryAddViewAPITestCase(APITestCase):
    fixtures = ['test_auth_data.json']

    def setUp(self):
        number_or_categories = 5
        for category_num in range(number_or_categories):
            Category.objects.create(title='Category %s' % category_num)

    def test_forbid_if_no_permission(self):
        resp = self.client.get(reverse('api_v1:create_category'))
        self.assertEqual(resp.status_code, 403)

    def test_create_parentcategory_with_permission(self):
        login = self.client.login(username='Contentmanager', password='unabomber45')
        resp = self.client.post(reverse('api_v1:create_category'), {'title': 'Для детей'})
        self.assertEqual(resp.status_code, 201)

    def test_create_category_with_permission(self):
        login = self.client.login(username='Contentmanager', password='unabomber45')
        category = {'title': 'Для детей',
                    'parent_id': {
                        'title': 'Category 1',
                        'id': 1
                    }}
        resp = self.client.post(reverse('api_v1:create_category'), json.dumps(category),
                                content_type='application/json')
        self.assertEqual(resp.status_code, 201)

    def test_create_category_without_permission(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.post(reverse('api_v1:create_category'), {'title': 'Для детей'})
        self.assertEqual(resp.status_code, 403)


class ArticleDeleteViewTestCase(APITestCase):
    fixtures = ['test_auth_data.json']

    def setUp(self):
        number_of_articles = 15
        for category_num in range(number_of_articles):
            Category.objects.create(title='Category %s' % category_num)

    def test_delete_category(self):
        old_categories_list = Category.objects.all()
        login = self.client.login(username='Contentmanager', password='unabomber45')
        resp = self.client.delete('/api/v1/categories/1/delete/')
        self.assertEqual(resp.status_code, 204)
        new_categories_list = Category.objects.all()
        self.assertTrue(len(old_categories_list) - 1, len(new_categories_list))

    def test_delete_with_no_permission(self):
        old_categories_list = Category.objects.all()
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.delete('/api/v1/categories/1/delete/')
        self.assertEqual(resp.status_code, 403)
        new_categories_list = Category.objects.all()
        self.assertTrue(len(old_categories_list), len(new_categories_list))


class CategoryListViewAPITestCase(APITestCase):
    def setUp(self):
        number_or_categories = 15
        for category_num in range(number_or_categories):
            Category.objects.create(title='Category %s' % category_num)

    def test_view_url_exists_at_desired_location(self):
        login = self.client.login(username='unabomber', password='unabomber67')
        resp = self.client.get('/api/v1/categories/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('api_v1:list_categories'))
        self.assertEqual(resp.status_code, 200)

    def test_serialized_data(self):
        resp = self.client.get(reverse('api_v1:list_categories'))
        self.assertEqual(resp.data, CategorySerializer(Category.objects.all(), many=True).data)


class ArticleListViewAPITestCase(APITestCase):
    fixtures = ['test_auth_data.json']

    def setUp(self):
        category = Category.objects.create(title='TestCategoryForArticle')
        number_of_articles = 15
        for article_num in range(number_of_articles):
            Article.objects.create(title='Category %s' % article_num, category_id=category, description='Blablablabla',
                                   image='images/vasserman.jpeg', user_id=User.objects.get(username='unabomber'))

    def test_view_url_exists_at_desired_location(self):
        resp = self.client.get('/api/v1/articles/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('api_v1:list_articles'))
        self.assertEqual(resp.status_code, 200)

    def test_serialized_data(self):
        resp = self.client.get(reverse('api_v1:list_articles'))
        self.assertEqual(len(resp.data), len(ArticleSerializer(Article.objects.all(), many=True).data))